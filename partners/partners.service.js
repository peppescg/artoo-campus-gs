angular.module('artoo').factory('Partners', function(){
    //modello partners affiliati
    var partner={
        company:'',
        vat_code:'',
        cod_fisc:'',
        address:'',
        zip_code:'',
        city:'',
        prov:'',
        country:'',
        email:'',
        username:'',
        pass:'',
        active:'',
        ultimo_acc:'',
        data_reg:'',
    };
    
    var getPartner=function(){
        return partner;
    };
    
    
    //public
    return {
        getPartner: getPartner,
    };
});